#!/bin/sh
# Usage: EMAIL=vasya@pupkin.ru NUM=1 GOST_KEY=1 sh key.sh
set -efu

cat << EOF > "x509_${NUM}.genkey.tpl"
[ req ]
prompt = no
string_mask = utf8only
distinguished_name = req_distinguished_name
x509_extensions = myexts
[ req_distinguished_name ]
organizationName = ROSA Linux
commonName = Kernel modules signing @ALGO@ key ${NUM}
emailAddress = ${EMAIL}
[ myexts ]
basicConstraints=critical,CA:FALSE
keyUsage=digitalSignature
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
EOF

sed -e 's,@ALGO@,RSA,g' "x509_${NUM}.genkey.tpl" > "x509_${NUM}.genkey.RSA"
sed -e 's,@ALGO@,GOST R 34.10-2012,g' "x509_${NUM}.genkey.tpl" > "x509_${NUM}.genkey.GOST"

_libressl_gen_key(){
	if [ "$GOST_KEY" = 1 ]
	then
		lssl_req_gost_args="\
			-newkey gost2001 \
			-pkeyopt dgst:streebog512 -pkeyopt paramset:A \
			-streebog512"
		OUT="full_key_GOST_${NUM}.pem"
		CONFIG="x509_${NUM}.genkey.GOST"
	else
		lssl_req_gost_args=""
		OUT="full_key_RSA_${NUM}.pem"
		CONFIG="x509_${NUM}.genkey.RSA"
	fi
		libressl req -new -nodes -utf8 -batch \
			$lssl_req_gost_args \
			-days 109500 \
			-x509 -config "$CONFIG" \
			-outform PEM \
			-out "$OUT" \
			-keyout "$OUT"

	# Verify
	if [ "$GOST_KEY" = 1 ]; then
		libressl x509 -in "full_key_GOST_${NUM}.pem" -text -noout \
			| grep -E 'Signature Algorithm:.*GOST R 34.10-2012'
		libressl x509 -in "full_key_GOST_${NUM}.pem" -text -noout \
			| grep -E 'Digest Algorithm:.*GOST R 34-11-2012'
		libressl x509 -in "full_key_GOST_${NUM}.pem" -text -noout \
			| grep -E 'Public Key Algorithm:.*GOST R 34.10-2012'
	fi
	
	sed -n '/^-----BEGIN CERTIFICATE-----$/,/^-----END CERTIFICATE-----$/p;/^-----END CERTIFICATE-----$/q' "$OUT" > "$(echo "$OUT" | sed -e 's,full_key_,public_key_,g')"
}

_libressl_gen_key
